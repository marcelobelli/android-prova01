package com.example.marcelo.prova01;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edStudentName;
    private EditText edStudentCel;
    private Boolean validEdStudentCel;
    private Boolean validEdStudentCelFirstNumber;
    private Boolean validEdStudentCelLength;
    private EditText edStudentEmail;
    private EditText edStudentRegistration;
    private RadioGroup rgCampus;
    private RadioButton rbChosenCampus;
    private List<CheckBox> cbInterests;
    private TextView tvStudentData;
    private Button btSaveStudent;
    private Button btSendEmail;
    private Button btSendSMS;
    private String errorMessage;
    private CheckBox cbAgreed;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeFields();
        listCheckBoxes();

        btSaveStudent.setEnabled(false);
        validEdStudentCel = isFilled(edStudentCel);
        validEdStudentCelFirstNumber = false;
        validEdStudentCelLength = minDigits();

        cbAgreed.setOnCheckedChangeListener(checkAgreed);


        edStudentCel.addTextChangedListener(edStudentCelWatcher);
        SaveButton();
        btSendEmail.setOnClickListener(new EnviarEmail());
        btSendSMS.setOnClickListener(new EnviarSMS());
    }

    @Override
    public void onClick(View v) {

        if (validateFields()) {
            studentData();
        }
    }

    private final TextWatcher edStudentCelWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            validEdStudentCel = isFilled(edStudentCel);
            validEdStudentCelLength = minDigits();
            if (validEdStudentCelLength) {
                validEdStudentCelFirstNumber = firstDigits();
            }
        }
    };

    private void SaveButton() {
        btSaveStudent.setOnClickListener(this);
    }

    private final CompoundButton.OnCheckedChangeListener checkAgreed = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                btSaveStudent.setEnabled(true);
            }
            else {
                btSaveStudent.setEnabled(false);
                tvStudentData.setText("");
            }
        }
    };

    private void initializeFields() {
        edStudentName = (EditText) findViewById(R.id.edStudentName);
        edStudentCel = (EditText) findViewById(R.id.edStudentCel);
        edStudentEmail = (EditText) findViewById(R.id.edStudentEmail);
        edStudentRegistration = (EditText) findViewById(R.id.edStudentRegistration);
        rgCampus = (RadioGroup) findViewById(R.id.rgCampus);
        tvStudentData = (TextView) findViewById(R.id.tvStudentData);
        btSaveStudent = (Button) findViewById(R.id.btSaveStudent);
        cbAgreed = (CheckBox) findViewById(R.id.cbAgreed);
        btSendEmail = (Button) findViewById(R.id.btSendEmail);
        btSendSMS = (Button) findViewById(R.id.btSendSMS);
    }

    private void listCheckBoxes() {
        cbInterests = new ArrayList<>();

        cbInterests.add((CheckBox) findViewById(R.id.cbInterestMovies));
        cbInterests.add((CheckBox) findViewById(R.id.cbInterestRead));
        cbInterests.add((CheckBox) findViewById(R.id.cbInterestSports));
        cbInterests.add((CheckBox) findViewById(R.id.cbInterestTheatre));
        cbInterests.add((CheckBox) findViewById(R.id.cbInterestTrips));
    }

    private Boolean isCheckBoxSelected() {
        for (CheckBox checkbox: cbInterests) {
            if (checkbox.isChecked()) {
                return true;
            }
        }
        return false;
    }

    private Boolean isFilled(EditText edittext) {
        if (edittext.getText().toString().isEmpty()) {
            return false;
        }
        return true;
    }

    private Boolean minDigits() {
        String phone = edStudentCel.getText().toString();

        if (phone.length() >= 8) {
            return true;
        }
        return false;
    }

    private Boolean firstDigits() {
        String phone = edStudentCel.getText().toString();
        char[] arrayPhone = phone.toCharArray();

        if (arrayPhone[0] == '8' || arrayPhone[0] == '9') {
            return true;
        }

        return false;
    }

    public Boolean validateFields () {

        if (!isFilled(edStudentName)) {
            errorMessage = this.getResources().getString(R.string.errorStudentName);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!validEdStudentCel) {
            errorMessage = this.getResources().getString(R.string.errorStudentCelEmpty);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!validEdStudentCelLength) {
            errorMessage = this.getResources().getString(R.string.errorStudentCelLength);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!validEdStudentCelFirstNumber) {
            errorMessage = this.getResources().getString(R.string.errorStudentCelFirstNumber);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isFilled(edStudentEmail)) {
            errorMessage = this.getResources().getString(R.string.errorStudentEmail);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isFilled(edStudentRegistration)) {
            errorMessage = this.getResources().getString(R.string.errorStudentRegistration);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (rgCampus.getCheckedRadioButtonId() == -1) {
            errorMessage = this.getResources().getString(R.string.errorCampus);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isCheckBoxSelected()) {
            errorMessage = this.getResources().getString(R.string.errorInterest);
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void studentData() {
        String studentInfo = "Nome: " + edStudentName.getText().toString() + "\n";
        studentInfo += "Celular: " + edStudentCel.getText().toString() + "\n";
        studentInfo += "E-mail: " + edStudentEmail.getText().toString() + "\n";
        studentInfo += "Matricula: " + edStudentRegistration.getText().toString() + "\n";

        int idRadioButton = rgCampus.getCheckedRadioButtonId();
        rbChosenCampus = (RadioButton) findViewById(idRadioButton);
        studentInfo += "Unidade: " + rbChosenCampus.getText().toString() + "\n";


        for (CheckBox checkbox: cbInterests) {
            if (checkbox.isChecked()) {
                studentInfo += "Interesse: " + checkbox.getText().toString() + "\n";
            }
        }

        tvStudentData.setText(studentInfo);
    }

    private class EnviarEmail implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            sendMail();
        }

        private void sendMail() {

            String message = tvStudentData.getText().toString();
            if (message.length() <= 0) return;

            String address = "luis.trevisan@up.edu.br";

            Intent intent = new Intent(Intent.ACTION_SEND);

//            intent.setData(Uri.parse("mailto:" + address));

            intent.putExtra(Intent.EXTRA_SUBJECT, "Resumo Formulário de " + edStudentName.getText().toString());

            intent.putExtra(Intent.EXTRA_TEXT, message);

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }


        }
    }

    private class EnviarSMS implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            sendSMS();
        }

        private void sendSMS() {
            String message = tvStudentData.getText().toString();
            if (message.length() <= 0) return;

            Intent intent = new Intent(Intent.ACTION_SENDTO);

            intent.setType("*/*");

            intent.setData(Uri.parse("smsto:999142324"));
            intent.putExtra("sms_body", message);

            intent.putExtra(Intent.EXTRA_STREAM, "");

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}
